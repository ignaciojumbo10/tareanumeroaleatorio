package com.example.loteria;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity {
    // declaramos las variables de los componentes
    private EditText numero;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // obtenemos los valores de la parte visual
        numero = findViewById(R.id.txtnumero);

    }

    public void Comprobar(View view){
        String num = numero.getText().toString();

        if(!num.equals("")){
            Intent intent = new Intent(this, Comprobacion.class);
            // envio el dato
            intent.putExtra("dato",num);
            startActivity(intent);
        }else{
            Toast.makeText(this,"Ingrese un dato", Toast.LENGTH_LONG).show();
            numero.requestFocus();
        }

    }








}