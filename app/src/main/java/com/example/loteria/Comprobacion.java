package com.example.loteria;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class Comprobacion extends AppCompatActivity {
    private TextView resultado;
    private String dato;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comprobacion);


        resultado = findViewById(R.id.txtresultado);
        comprobar();

    }

    // metodo para comprobar los datos
    public void comprobar(){
         dato = getIntent().getStringExtra("dato");

        int num = Integer.parseInt(dato);
        int nAleatorio = (int) (Math.random() * 10);
        if(num == nAleatorio){
            resultado.setText("!!!!!!BOOYAH!!!!!! \nGanastes");

        }else{
            resultado.setText("Perdistes. \nEl numero aleatorio era: " + nAleatorio);
        }
    }

    public void VolverJugar(View view){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }


}